#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "RU");
	shared_ptr<ANeuralNetwork> neuroNet = CreateNeuralNetwork();
	
	neuroNet->Load("EducationResult");

	cout << "Neural network type is:" << endl << neuroNet->GetType().c_str() << endl;

	vector < vector<float>> inputs, outputs;

	bool successLoading = LoadData("EducationData", inputs, outputs);

	if (!successLoading)
	{
		cout << "Loading error!" << endl;
		return 0;
	}

	cout << "������� ������ ���������:" << endl;

	for (int i = 0; i < inputs.size(); i++)
	{
		for (int j = 0; j < inputs[i].size(); j++)
			cout << inputs[i][j] << " ";
		cout << endl;
	}


	cout << "��������� �������� ������ ���������:" << endl;

	for (int i = 0; i < outputs.size(); i++)
	{
		for (int j = 0; j < outputs[i].size(); j++)
			cout << outputs[i][j] << " ";
		cout << endl;
	}

	cout << "����������� �������� ������ ���������:" << endl;

	for (int i = 0; i < inputs.size(); i++)
		outputs[i] = neuroNet->Predict(inputs[i]);

	for (int i = 0; i < outputs.size(); i++)
	{
		for (int j = 0; j < outputs[i].size(); j++)
			cout << outputs[i][j] << " ";
		cout << endl;
	}

	return 0;


}