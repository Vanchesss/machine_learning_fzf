#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<size_t> vec = { 2,10,10,1 }; // ���������� �������� � 4-� �����
	//shared_ptr - ����� ���������� ������, ����� ������� ������ � ������ ����� ������� ������ ����� �������� ��� �����
	//unique_ptr - ������ ���������� ������
	shared_ptr<ANeuralNetwork> neuroNet = CreateNeuralNetwork(vec);
	vector<vector<float>> inputs, outputs;
	bool success = LoadData("EducationData", inputs, outputs);
	if (!success)
	{
		cout << "Loading error!" << endl;
		return 0;
	}
	BackPropTraining(neuroNet, inputs, outputs, 15000, 10.e-2, 0.1, true);
	neuroNet -> Save("EducationResult");
	cout << "Education complete!" << endl;
	return 0;
}